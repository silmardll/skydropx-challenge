require 'fedex'
require_relative '../presenters/fedex_tracking_presenter'
require_relative '../null_tracking_information'

class FedexTracker
  attr_reader :tracking_number

  def initialize(tracking_number)
    @tracking_number = tracking_number
  end

  def track
    results = tracker.track(tracking_number: tracking_number)
    FedexTrackingPresenter.new(results.first)
  rescue Fedex::RateError => e
    NullTrackingInformation.new(tracking_number, e.message)
  end

  private

  def tracker
    Fedex::Shipment.new(
      key:            ENV['FEDEX_KEY'],
      password:       ENV['FEDEX_PASSWORD'],
      account_number: ENV['FEDEX_ACCOUNT_NUMBER'],
      meter:          ENV['FEDEX_METER'],
      mode:           ENV['FEDEX_MODE'])
  end
end
