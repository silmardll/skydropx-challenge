class TrackingNotificator
  attr_reader :tracking

  def initialize(tracking)
    @tracking = tracking
  end

  def notify
    # notifica a la aplicación 'A'
    # cada objeto 'tracking' debe responder a los métodos (valores) que la
    # applicacion 'A' espera recibir, por ejemplo:
    # tracker, tracking_number, status, status_code, etc.
  end
end
