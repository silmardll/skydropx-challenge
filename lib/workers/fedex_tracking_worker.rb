require 'sidekiq'
require_relative '../trackers/fedex_tracker'
require_relative '../tracking_notificator'

class FedexTrackingWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'fedex'

  def perform(tracking_number)
    fedex    = FedexTracker.new(tracking_number)
    tracking = fedex.track

    TrackingNotificator.new(tracking).notify
  end
end
