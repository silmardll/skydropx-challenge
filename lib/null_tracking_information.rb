class NullTrackingInformation
  attr_reader :tracking_number, :status

  def initialize(tracking_number, status)
    @tracking_number = tracking_number
    @status          = status
  end

  def status_code
    'EXCEPTION'
  end

  def retry?
    status !~ /This tracking number cannot be found/
  end
end
