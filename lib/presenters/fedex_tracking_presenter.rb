class FedexTrackingPresenter
  attr_reader :tracking

  def initialize(tracking)
    @tracking = tracking
  end

  def number
    tracking.tracking_number
  end

  def status
    tracking.status
  end

  def retry?
    false
  end

  def status_code
    case tracking.status_code
    when 'OC'
      'CREATED'
    when 'FD', 'SF', 'DP', 'AP', 'OD'
      'ON_TRANSIT'
    when 'DL'
      'DELIVERED'
    when 'EXCEPTION'
      'EXCEPTION'
    end
  end
end
