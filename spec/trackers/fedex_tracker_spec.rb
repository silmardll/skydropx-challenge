require 'spec_helper'
require 'trackers/fedex_tracker'

RSpec.describe FedexTracker do
  let(:tracker) { described_class.new(tracking_number) }

  context '#track' do
    context 'cuando el rastreo esta vigente' do
      let(:tracking_number) { '449044304137821' }
      it 'regresa una instancia de tipo FedexTrackingPresenter' do
        result = tracker.track

        expect(result).to be_a(FedexTrackingPresenter)
      end
    end

    context 'cuando el rastreo ha expirado' do
      let(:tracking_number) { '390321041908' }
      it 'regresa una instancia de tipo NullTrackingInformation' do
        result = tracker.track

        expect(result).to be_a(NullTrackingInformation)
      end
    end
  end
end

#fedex = Fedex::Shipment.new(key: 'O21wEWKhdDn2SYyb', password: 'db0SYxXWWh0bgRSN7Ikg9Vunz', account_number: '510087780', meter: '119009727', mode: 'test')

#["OC", "FD", "SF", "DP", "AP", "OD", "DL"]
#["Shipment information sent to FedEx", "At FedEx destination facility", "At destination sort facility", "Departed FedEx location", "At Pickup", "On FedEx vehicle for delivery", "Delivered"]
#["449044304137821", "920241085725456", "568838414941", "039813852990618", "149331877648230", "231300687629630", "122816215025810", "390321041908"]
