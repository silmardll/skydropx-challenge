require 'spec_helper'
require 'ostruct'
require 'presenters/fedex_tracking_presenter'

RSpec.describe FedexTrackingPresenter do
  let(:presenter) { described_class.new(tracking) }
  let(:tracking) do
    OpenStruct.new(
      tracking_number: nil,
      status:          nil,
      status_code:     nil
    )
  end

  describe '#number' do
    it 'regresa el "tracking number"' do
      tracking.tracking_number = '449044304137821'
      expect(presenter.number).to eq '449044304137821'
    end
  end

  describe '#status' do
    it 'regresa el estado del rastero' do
      tracking.status = 'Shipment information sent to FedEx'
      expect(presenter.status).to eq 'Shipment information sent to FedEx'
    end
  end

  describe '#retry?' do
    it 'regresa false' do
      expect(presenter.retry?).to eq false
    end
  end

  describe '#status_code' do
    context 'cuando el código de estado del rastreo es "OC"' do
      it 'regresa "CREATED"' do
        tracking.status_code = 'OC'
        expect(presenter.status_code).to eq 'CREATED'
      end
    end

    context 'cuando el código de estado del rastreo es "FD"' do
      it 'regresa "ON_TRANSIT"' do
        tracking.status_code = 'FD'
        expect(presenter.status_code).to eq 'ON_TRANSIT'
      end
    end

    context 'cuando el código de estado del rastreo es "SF"' do
      it 'regresa "ON_TRANSIT"' do
        tracking.status_code = 'SF'
        expect(presenter.status_code).to eq 'ON_TRANSIT'
      end
    end

    context 'cuando el código de estado del rastreo es "DP"' do
      it 'regresa "ON_TRANSIT"' do
        tracking.status_code = 'DP'
        expect(presenter.status_code).to eq 'ON_TRANSIT'
      end
    end

    context 'cuando el código de estado del rastreo es "AP"' do
      it 'regresa "ON_TRANSIT"' do
        tracking.status_code = 'DP'
        expect(presenter.status_code).to eq 'ON_TRANSIT'
      end
    end

    context 'cuando el código de estado del rastreo es "OD"' do
      it 'regresa "ON_TRANSIT"' do
        tracking.status_code = 'OD'
        expect(presenter.status_code).to eq 'ON_TRANSIT'
      end
    end

    context 'cuando el código de estado del rastreo es "DL"' do
      it 'regresa "DELIVERED"' do
        tracking.status_code = 'DL'
        expect(presenter.status_code).to eq 'DELIVERED'
      end
    end

    context 'cuando el código de estado del rastreo es "EXCEPTION"' do
      it 'regresa "EXCEPTION"' do
        tracking.status_code = 'EXCEPTION'
        expect(presenter.status_code).to eq 'EXCEPTION'
      end
    end
  end
end
