require 'spec_helper'
require 'null_tracking_information'

RSpec.describe NullTrackingInformation do
  let(:tracking) { described_class.new(tracking_number, status) }
  let(:tracking_number) { '390321041908' }
  let(:status) do
    'This tracking number cannot be found. Please check the number or contact the sender.'
  end

  describe '#tracking_number' do
    it 'regresa el "tracking number"' do
      expect(tracking.tracking_number).to eq tracking_number
    end
  end

  describe '#status' do
    it 'regresa el mensaje de error como estado' do
      expect(tracking.status).to eq status
    end
  end

  describe '#status_code' do
    it 'regresa "EXCEPTION" como código de estado' do
      expect(tracking.status_code).to eq 'EXCEPTION'
    end
  end

  describe  '#retry?' do
    context 'cuando el mensaje de error es de un rastreo que ha expirado' do
      it 'regresa false' do
        expect(tracking.retry?).to eq false
      end
    end

    context 'cuando el mensaje de error es diferente al de un rastreo que ha expirado' do
      let(:status) { 'timeout' }
      it 'regresa true' do
        expect(tracking.retry?).to eq true
      end
    end
  end
end
