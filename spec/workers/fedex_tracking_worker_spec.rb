require 'spec_helper'
require 'workers/fedex_tracking_worker'
require 'tracking_notificator'

RSpec.describe FedexTrackingWorker do
  it 'encola un trabajo de rastreo vigente' do
    expect(FedexTracker).to receive(:new)
      .with('449044304137821')
      .and_call_original

    expect(TrackingNotificator).to receive(:new)
      .with(be_a(FedexTrackingPresenter))
      .and_call_original

    expect {
      described_class.perform_async('449044304137821')
    }.to change(described_class.jobs, :size).by(1)

    described_class.drain
  end

  it 'encola un trabajo de rastreo expirado' do
    expect(FedexTracker).to receive(:new)
      .with('390321041908')
      .and_call_original

    expect(TrackingNotificator).to receive(:new)
      .with(be_a(NullTrackingInformation))
      .and_call_original

    expect {
      described_class.perform_async('390321041908')
    }.to change(described_class.jobs, :size).by(1)

    described_class.drain
  end
end
