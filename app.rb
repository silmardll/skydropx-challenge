require 'sinatra'
require_relative './lib/workers/fedex_tracking_worker'

get '/' do
  'running!'
end

get '/start_workers' do
  # por simplicidad y como ejemplo se inician los trabajos aquí
  # podría estar como una tarea y usar crontab, pero va a depender mucho de si la aplicación
  # 'A' le indica a la aplicación 'B' o la aplicación 'B' le pregunta a la aplicación 'A'
  # que paquetes rastrear

  (get_tracking_numbers * 5_000).each do |tracking_number|
    FedexTrackingWorker.perform_async(tracking_number)
  end

  '50_000 jobs encolados'
end

def get_tracking_numbers
  %w[
    449044304137821
    920241085725456
    568838414941
    039813852990618
    149331877648230
    231300687629630
    122816215025810
    390321041908
    390321827777
    390348753734
  ]
end
