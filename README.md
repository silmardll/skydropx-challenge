# Backend Coding Challenge - SkydropX

#### Fedex credentials
- `cp .env.template .env`
- add the credentials values
#### Generate the docker image
- docker-commpose build
#### Start services
- `docker-compose up -d`
#### Run test suite
- `docker-compose exec web rspec`
#### URL sidekiq web
- http://localhost:4567/sidekiq/
#### URL create jobs
- http://localhost:4567/start_workers
