require './app.rb'
require 'sidekiq/web'

REDIS_POOL = ConnectionPool.new(size: 30) { Redis.new(url: ENV['REDIS_URL']) }

Sidekiq.configure_server do |config|
  config.redis = REDIS_POOL
end

Sidekiq.configure_client do |config|
  config.redis = REDIS_POOL
end

run Rack::URLMap.new('/' => Sinatra::Application, '/sidekiq' => Sidekiq::Web)
